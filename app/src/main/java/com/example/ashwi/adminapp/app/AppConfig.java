package com.example.ashwi.adminapp.app;

public class AppConfig {
    // Server user login url
    public static String URL_LOGIN = "http://www.dcnpackagemsc2k16.info/admin_api/login.php";

    // Server user register url
    public static String URL_REGISTER = "http://www.dcnpackagemsc2k16.info/admin_api/register.php";

    public static String URL_ADDBRANCH = "http://www.dcnpackagemsc2k16.info/admin_api/addbranch.php";
}