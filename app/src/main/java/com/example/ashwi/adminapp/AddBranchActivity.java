package com.example.ashwi.adminapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.example.ashwi.adminapp.app.AppConfig;
import com.example.ashwi.adminapp.app.AppController;
import com.example.ashwi.adminapp.R;

public class AddBranchActivity extends Activity {

    private static final String TAG =AddBranchActivity.class.getSimpleName();
    private Button btnLogin;
    private EditText e1;
    private EditText e2;
    private EditText e3;
    private EditText e4;
    private EditText e5;
    private EditText e6;
    private EditText e7;
    private EditText e8;
    private EditText e9;
    private EditText e10;
    private EditText e11;
    private ProgressDialog pDialog;
    public int f1=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_branch);

        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        e3 = (EditText) findViewById(R.id.e3);
        e4 = (EditText) findViewById(R.id.e4);
        e5 = (EditText) findViewById(R.id.e5);
        e6 = (EditText) findViewById(R.id.e6);
        e7 = (EditText) findViewById(R.id.e7);
        e8 = (EditText) findViewById(R.id.e8);
        e9 = (EditText) findViewById(R.id.e9);
        e10 = (EditText) findViewById(R.id.e10);
        e11 = (EditText) findViewById(R.id.e11);
        btnLogin = (Button) findViewById(R.id.btnProceed);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                /*Toast.makeText(getApplicationContext(),
                        "Please enter the credentials!", Toast.LENGTH_LONG)
                        .show();

                Intent intent = new Intent(AddBranchActivity.this, SuperAdminActivity.class);
                startActivity(intent);
                finish();*/
                String i1 = e1.getText().toString();
                String i2 = e2.getText().toString();
                String i3 = e3.getText().toString();
                String i4 = e4.getText().toString();
                String i5 = e5.getText().toString();
                String i6 = e6.getText().toString();
                String i7 = e7.getText().toString();
                String i8 = e8.getText().toString();
                String i9 = e9.getText().toString();
                String i10 = e10.getText().toString();
                String i11 = e11.getText().toString();

                if(i10.equals(i11))
                    registeruser(i1,i2,i3,i4,i5,i6,i7,i8,i9,i10);
                else
                    Toast.makeText(getApplicationContext(),
                            "Passwords don't match!", Toast.LENGTH_LONG)
                            .show();

                /*if(f1==1)
                    addbranch(i1,i2,i3,i4,i5,i6,i8);*/
            }

        });

    }
    private void registeruser(final String e1,final String e2,final String e3,final String e4,final String e5,final String e6,final String e7, final String e8 , final String e9, final String e10) {
        String tag_string_req = "req_register";

        pDialog.setMessage("ADDING BRANCH ADMIN...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (error==false) {
                        addbranch(e1,e2,e3,e4,e5,e6,e8);
                        f1=1;

                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", e7);
                params.put("id", e8);
                params.put("mno", e9);
                params.put("password", e10);
                params.put("adminlevel","1");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void addbranch(final String e1,final String e2,final String e3,final String e4,final String e5,final String e6,final String e8)
    {
        String tag_string_req = "req_register";


        pDialog.setMessage("ADDING BRANCH DETAILS...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_ADDBRANCH, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "ADD BRANCH Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {

                        Intent intent = new Intent(AddBranchActivity.this, SuperAdminActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "branch Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", e1);
                params.put("address", e2);
                params.put("district", e3);
                params.put("state", e4);
                params.put("country",e5);
                params.put("pincode",e6);
                params.put("id",e8);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
