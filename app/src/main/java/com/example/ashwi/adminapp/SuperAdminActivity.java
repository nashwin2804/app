package com.example.ashwi.adminapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SuperAdminActivity extends AppCompatActivity {

    private Button AddBranch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_super_admin);

        AddBranch=(Button)findViewById(R.id.addBranch);
        AddBranch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent intent = new Intent(SuperAdminActivity.this,
                        AddBranchActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
